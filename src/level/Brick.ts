export class Brick {

    /** Amount of hits the brick can take.*/
    private strength:number;

    /** Color strings for visual indicator of brick strength
     *  [0] = background color, [1] = green, [2] = yellow, [3] = orange, [4] = red, [5] = grey
     */
    static tints:string[] = ["341d3f", "6de847","e8e847","e89c47","e85147", "888e96"];

    /** Width of a brick, % of screen width */
    public width:number;
    /** Height of a brick, 1/3 of brick width */
    public height:number;
    /** Amount of space between each brick */
    public buffer:number;

    /** Maximum strength a brick can be, any greater is considered an unbreakable brick */
    private maxStrength:number = 4

    /** Where the brick should move towards during update()*/
    public targetPos:Phaser.Point;

    constructor( public level:Phaser.State, private sprite:Phaser.Sprite ){

        // Enable physics

        level.game.physics.enable( sprite, Phaser.Physics.ARCADE );
        sprite.body.collideWorldBounds = true;
        sprite.body.bounce.set( 1 );
        sprite.body.immovable = true;

        // Set size

        this.width = ( level.game.width * 0.8 )/8;
        this.height = this.width/3;
        this.buffer = this.width * 0.1;
    }

    /** Move the brick towards its target position and adjust its width and height */
    public update(){
        this.moveTo( this.targetPos.x, this.targetPos.y );
        this.sprite.width = this.width;
        this.sprite.height = this.height;
    }

    /**
     * Lerp the brick towards x and y
     * @param x horizontal coordinate to move to
     * @param y vertical coordinate to move to
     */
    public moveTo( x:number, y:number ){
        this.sprite.x = Phaser.Math.linearInterpolation( [ this.sprite.x, x ], 0.2 );
        this.sprite.y = Phaser.Math.linearInterpolation( [ this.sprite.y, y ], 0.2 );
    }

    /**
     * Sets the bricks strength to the strength param and adjusts color
     * @param strength value to set strength of brick to
     */
    public setStrength( strength:number ){

        this.strength = strength;
        this.sprite.tint = parseInt( Brick.tints[ this.strength ], 16 );

        if( this.strength === 0 ){
            this.sprite.kill();
            return;

        } else if( !this.sprite.alive ){
            this.sprite.revive();

        }
    }

    /** Returns the strength of this brick */
    public getStrength():number{
        return this.strength;
    }

    /** Returns the maximum strength of this brick */
    public getMaxStrength():number{
        return this.maxStrength;
    }

    /** Returns the sprite associated with this brick */
    public getSprite():Phaser.Sprite{
        return this.sprite;
    }

    /** Destroy the sprite associated with this brick */
    public destroy(){
        this.sprite.kill();
        this.sprite.destroy();
    }
}

