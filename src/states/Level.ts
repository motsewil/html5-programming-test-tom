import {Ball} from '../level/Ball'
import {Paddle} from '../level/Paddle'
import {Brick} from '../level/Brick'
import {Breakout} from '../Breakout'


/**
 *  Enum for whether a brick layout should be left or centre aligned
 */
export enum LevelFormat{
    CENTRE,
    LEFT
} 

/**
 * State for the core gameplay loop
 */
export class Level extends Phaser.State {
    
    private demoLevel = {
        format: LevelFormat.CENTRE,
        bricks:
            [   
                [4,4,4,4,4,4],
                [3,0,0,0,0],
                [2,2,2,2],
                [1,1,1]
            ]
    };

    private paddle:Paddle;
    private ball:Ball;

    /** The game.time.now upon this states creation */
    public createTime:number;
    
    // UI Objects for resetting the game by holding ESC

    /** Time until exit to menu */
    private resetTime:number;
    private resetText:Phaser.Text;
    private resetTimeText:Phaser.Text;

    // Button and text for navigating to custom level creation

    private customButton:Phaser.Button;
    private customText:Phaser.Text;

    // UI Objects for displaying player Score and Lives remaining
    private livesText:Phaser.Text;
    private scoreText:Phaser.Text;
    /** The score that will be accumulated if the player successfully lands the ball on the paddle */
    private tentativeScore:Phaser.Text;
    /** Lives remaining */
    private lives:number;
    /** Score currently saved */
    private score:number;

    /** 2D Array for holding all the brick objects that comprise a level in [rows][cols] */
    private bricks:Brick[][] = [];

    /** The index for which level layout should be loaded, custom or otherwise */
    private levelIndex:number = 0;

    /** Phaser.Group for all UI elements in order to bring them to the top of the screen when new level layouts are instantiated */
    private ui:Phaser.Group;

    create() {
        
        this.createTime = this.time.now;
        this.levelIndex = 0;
        
        this.paddle = new Paddle( this, this.game.add.sprite( this.game.width/2, this.game.height * 0.9, 'brick' ) );
        this.ball = new Ball( this, this.game.add.sprite( this.paddle.getSprite().x, this.getPaddle().getSprite().y, 'ball' ), this.paddle );
        
        this.physics.startSystem(Phaser.Physics.ARCADE);
        this.physics.arcade.checkCollision.down = false;

        // reset text display

        this.resetText = this.game.add.text( this.game.width/2, this.game.height/2, "Exiting game in", { fill:"white" } );
        this.resetText.alpha = 0;
        this.resetText.anchor = new Phaser.Point( 0.5, 0.5 );
        this.resetText.rotation = Math.PI*0.02;
        this.resetTimeText = this.game.add.text( this.resetText.x, this.resetText.y + this.game.height*0.05, "5", { fill:"white" } );
        this.resetTimeText.anchor = this.resetText.anchor;
        this.resetTimeText.alpha = 0;

        // Custom Level Navigation display

        this.customButton = this.game.add.button( this.game.width* 0.95, this.game.height*0.05, 'brick', this.loadEditor, this);
        this.customText = this.game.add.text( this.game.width* 0.95, this.game.height*0.05, "Level editor", {fill:"none"} );
        this.customText.anchor = new Phaser.Point( 1, 0.5 );
        this.customButton.anchor = this.customText.anchor;
        this.customButton.width = this.customText.width;
        this.customButton.height = this.customText.height;
        this.customButton.alpha = 0.2;

        // Lives and Score display

        this.livesText = this.game.add.text( this.game.width * 0.05, this.game.height * 0.05, "Lives remaining : 3", { fill:"white" } );
        this.scoreText = this.game.add.text( this.game.width * 0.05, this.game.height * 0.05 + this.livesText.height*1.2 , "Score : 0", { fill:"white" } );
        this.tentativeScore = this.game.add.text( this.game.width * 0.05, this.game.height * 0.05 + this.livesText.height*2.4 , "", { fill:"white" } );
        
        // Add UI elements to the UI Group

        this.ui = this.game.add.group( this.game.world );
        this.ui.add( this.livesText );
        this.ui.add( this.scoreText );
        this.ui.add( this.tentativeScore );
        this.ui.add( this.customButton );
        this.ui.add( this.customText );
        this.ui.add( this.resetTimeText );
        this.ui.add( this.resetText );

        // Initialise level layout

        this.lives = 3;
        this.score = 0;
        this.loadNextLevel();
    }

    update(){

        // Small delay before the level starts to prevent issues with unexpected immediate input

        if( this.time.now - this.createTime < 100 ){
            return;
        }

        this.checkQuit();
        
        // Update all non UI gameobjects

        this.paddle.update();
        this.ball.update();
        this.bricks.forEach( row => {
            row.forEach( brick => {
                brick.update();
            })
        });

        // Physics

        this.checkCollisions();
    }

    /** Using the current level index load the next available level for the player.
     *  If the premade levels are exhausted, reset level index and switch to custom levels
     *  If the levels are exhausted return to menu
     */
    private loadNextLevel(){

        this.levelIndex++;


        // Destroy all remaining brick objects in the existing level

        for( let row = 0; row < this.bricks.length; row++ ){
            for( let col = 0; col < this.bricks[ row ].length; col++ ){
                let brick = this.bricks[ row ][ col ];
                brick.destroy();
            }
            this.bricks[ row ] = [];
        }
        this.bricks = [];

        // If the player has finished all premade levels, switch to custom levels

        if( this.levelIndex > ( this.game as Breakout ).levelCount ) {
            localStorage.setItem( "custom-first", '1' );
            this.levelIndex = 1;
        }

        
        // Get the level either from the premade levels in the game cache or from local storage depending on custom or premade

        let levelToLoad;
        if( localStorage.getItem( "custom-first" ) !== null && localStorage.getItem( "custom-first" ) === "1" ){
            levelToLoad = this.getCustomLevel();
        } else {
            levelToLoad = this.game.cache.getJSON( "level-" + this.levelIndex );
        }

        // If there are no levels loaded by this point the player has exhausted all available levels

        if( levelToLoad === null ){
            this.gameOverWin();
            return;
        }

        let format = levelToLoad.format;
        let rows = levelToLoad.bricks;

        // Instantiate each brick from rows into this.bricks and calculate their desired position

        for( let row = 0; row < rows.length; row++ ){
            this.bricks[ row ] = [];
            for( let col = 0; col < rows[ row ].length; col++ ){
                let brick = new Brick( this, this.game.add.sprite( this.game.width/2, 0, 'brick' ) );
                let sprite = brick.getSprite();
                sprite.anchor = new Phaser.Point( 0.5, 0.5 );
                brick.setStrength( rows[ row ][ col ] );

                // Calculate position
                let offsetX = 0;
                let initPointX = 0;
                let amountInRow = rows[ row ].length;
                let bricksFromCentre = col/2;

                // Check whether the level is centre or left aligned

                if( format === LevelFormat.CENTRE ){

                    initPointX = this.game.width/2;

                    // If their are an even amount of bricks in this row shift the offset by half a brick width

                    if( amountInRow % 2 === 0 ){
                        offsetX = ( brick.width + brick.buffer ) * Math.floor( bricksFromCentre );
                        offsetX += brick.width/2 + brick.buffer/2;
                    } else {
                        offsetX = ( brick.width + brick.buffer ) * Math.round( bricksFromCentre );
                    }

                    // If the brick should be on the left side of the level, reverse its offset

                    if( col % 2 === 0 ){
                        offsetX *= -1;
                    }

                } else if( format === LevelFormat.LEFT ){

                    // Offset is set to brick+buffer width times the column of the brick

                    initPointX = this.game.width * 0.1  - brick.width;
                    offsetX = brick.width + brick.buffer;
                    offsetX *= col+1;

                }
                brick.targetPos = new Phaser.Point( initPointX + offsetX, brick.height*2 + ( brick.height + brick.buffer ) * ( row+1 ) );
                this.bricks[ row ][ col ] = brick;
            }
        }

        // Bring the UI back to the top so the bricks do not sit on top of them

        this.game.world.bringToTop( this.ui );

    }

    /** Retrieve the relevant custom level for the player */
    private getCustomLevel(){
        if( localStorage.getItem("custom-level-count") === null || this.levelIndex > parseInt( localStorage.getItem( "custom-level-count" ) ) ){
            return null;
        } else {
            return JSON.parse( localStorage.getItem("custom-level-" + this.levelIndex ) );
        }
    }

    /** Check for any collisions for the ball against the players paddle and all bricks in the scene */
    private checkCollisions(){
        this.physics.arcade.collide( this.ball.getSprite(), this.paddle.getSprite(), this.ballHitPaddle, null, this);

        for ( let i = 0; i < this.bricks.length; i++ ) {
            let row = this.bricks[i];
            for ( let index = 0; index < row.length; index++ ) {
                let brick = row[ index ];
                this.physics.arcade.collide( this.ball.getSprite(), brick.getSprite(), this.ballHitBrick, null, this );
            }
        }
        
    }

    /** Called when the ball falls off the bottom of the screen.
     *  Lowers the players lives and score and triggers game over if lives are out
     */
    public ballLost(){
        this.score -= 200;
        this.lives -= 1;
        this.scoreText.text = "Score : " + this.score.toString();
        this.livesText.text = "Lives : " + this.lives;
        this.ball.reset();
        if( this.lives <= 0 ){
            this.gameOver();
        }
    }

    /** Called whenever the ball hits one of the bricks on screen.
     *  Checks for level completion and loads next level if relevant
     */
    private ballHitBrick( ballSprite, brickSprite ){

        for ( let i = 0; i < this.bricks.length; i++ ) {
            let row = this.bricks[i];

            for ( let index = 0; index < row.length; index++ ) {
                let brick = row[ index ];

                // If this brick is the same as the brick hit have it take damage and check for a win. 

                if( brick.getSprite() === brickSprite ){
                    if( brick.getStrength() <= brick.getMaxStrength() ){

                        brick.setStrength( brick.getStrength() - 1 );
                        this.tentativeScore.text = "+" + this.ball.hitBrick();

                        if( this.checkWin() ){

                            this.score += this.ball.retrieveScore() + 10000;
                            this.scoreText.text = "Score : " + this.score.toString();
                            this.tentativeScore.text = "";
                            this.ball.reset();
                            this.loadNextLevel();

                        }
                    }
                    return;
                }
            }
        }

    }

    /** Called whenever the ball hits the paddle.
     *  Accumulates the score the ball gained from hitting bricks and calculates the balls new direction
     */
    private ballHitPaddle(){
        let diff = 0;
        let ball = this.ball.getSprite();
        let paddle = this.paddle.getSprite();

        this.score += this.ball.retrieveScore();
        this.scoreText.text = "Score : " + this.score.toString();
        this.tentativeScore.text = "";

        // Calculate balls new direction and velocity

        if ( ball.x < paddle.x ){
            diff = paddle.x - ball.x;
            ball.body.velocity.x = (-10 * diff);
        } else if ( ball.x > paddle.x ){
            diff = ball.x -paddle.x;
            ball.body.velocity.x = (10 * diff);
        } else {
            ball.body.velocity.x = 2 + Math.random() * 8;
        }
    }

    /** Checks whether there are any breakable bricks left on the screen. 
     *  Returns true if none are found and level is complete
     *  Returns false if any breakable bricks are found
     */
    private checkWin():boolean{
        for ( let i = 0; i < this.bricks.length; i++ ) {
            let row = this.bricks[i];
            for ( let index = 0; index < row.length; index++ ) {
                let brick = row[ index ];
                if( brick.getStrength() > 0 && brick.getStrength() < 5 ) {
                    return false;
                }
            }
        }
        return true;
    }

    /** Checks whether the game should quit or not */
    private checkQuit(){

        // If the player is holding down the ESC key, display and shake the reset text.

        if( this.game.input.keyboard.isDown( Phaser.KeyCode.ESC ) ){
            this.resetText.alpha = 1 - this.resetTime/4000;
            this.resetTime -= this.time.elapsed;
            this.resetTimeText.text = ( Math.floor( this.resetTime/1000 ) ) + "s";
            this.resetTimeText.alpha = this.resetText.alpha;
            
            // Shake the text for some pizzazz
            if( this.resetTime % 50 === 0 ){
                this.resetText.rotation = -this.resetText.rotation;
            }

            // If the reset time has gone below zero, quit to menu

            if( Math.floor( this.resetTime/1000 ) <= 0 ){
                this.game.state.start( "MainMenu" );
            }

        } else {
            this.resetTime = 4000;
            this.resetText.alpha = 0;
            this.resetTimeText.alpha = 0;

        }
    }

    /** Called when the game is either lost or won and the game should quit to menu
     *  Saves the players current score in localStorage
     */
    public gameOver(){
        let save = localStorage.getItem( "highscores" );
        let highScores = [];
        if( save !== null ){
            highScores = JSON.parse( save ).scores;
        }
        highScores[ highScores.length ] = this.score;
        highScores.sort();
        localStorage.setItem( "highscores", JSON.stringify( { scores:highScores} ));
        this.game.state.start( "MainMenu" );
    }

    /** Called when the player wins the game! Ideally this should also delay the game over until a winscreen is shown */
    private gameOverWin(){
        this.score += 50000;
        this.gameOver();
    }

    /** Gets the paddle on this level object */
    public getPaddle(){
        return this.paddle;
    }

    /** Switches state to the level editor */
    private loadEditor(){
        this.game.state.start( "Editor" );
    }
}

