import {Breakout} from '../Breakout'
export class Preload extends Phaser.State {
    
    preload(){
        localStorage.setItem("custom-first", "0");
        this.load.image( "brick" , "assets/brick.png" );
        this.load.image( "ball", "assets/ball.png" );
        this.load.json( "level-1", "assets/levels/level-1.json");
        this.load.json( "level-2", "assets/levels/level-2.json");
        this.load.json( "level-3", "assets/levels/level-3.json");
        ( this.game as Breakout ).levelCount = 3;
    }

    create() {
        console.log( "state started : " + this.game.state.getCurrentState().key );
        this.game.state.start( "MainMenu" );
    }

    update(){

    }
}