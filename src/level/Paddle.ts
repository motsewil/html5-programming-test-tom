import {Ball} from './Ball'
import {Level} from '../states/Level'

/** Class to manage the players paddle */
export class Paddle {

    /** Width of the players paddle, % of game.width */
    private paddleWidth:number = this.level.game.width/7;
    /** Max speed of the paddle */
    private paddleMoveSpeed:number = 2.5;

    /** Last known position of the mouse */
    private lastKnownMousePos:number;
    /** time.now from when the mouse was last moved */
    private lastMoveTimeofMouse:number;

    /** Target x coordinate for the paddle to be */
    private targetPos:number;

    /** Ball Object for the paddle to keep track off */
    private ball:Ball;

    constructor( public level:Level, private sprite:Phaser.Sprite ){
        
        /** Set size and color of the ball */
        sprite.anchor.setTo(0.5, 0.5);
        sprite.width = this.paddleWidth;
        sprite.height = sprite.width/10;
        sprite.tint = parseInt("efb265", 16);

        /** Enable physics */
        level.game.physics.enable( sprite, Phaser.Physics.ARCADE );
        sprite.body.collideWorldBounds = true;
        sprite.body.bounce = true;
        sprite.body.immovable = true;

        this.targetPos = this.sprite.x;

    }

    /** Handle all movement input via mouse and keyboard and lerp the sprite towards that position */
    public update(){
        this.handleInput();
        this.sprite.x = Phaser.Math.linearInterpolation( [ this.sprite.x, this.targetPos ], this.paddleMoveSpeed * this.level.game.time.physicsElapsed );
    }

    /** Retrieves relevant input fromt the game and adjusts the paddles target position accordingly */
    private handleInput(){
        
        // Prioritize keyboard over mouse

        if( this.level.game.input.keyboard.isDown( Phaser.KeyCode.LEFT ) ) {

            // Target position 100px to left

            let newX:number = this.sprite.x - 100;
            this.targetPos = newX;
            this.sprite.x = Phaser.Math.linearInterpolation( [ this.sprite.x, newX ], this.paddleMoveSpeed * this.level.game.time.physicsElapsed );
            return;
        } else if( this.level.input.keyboard.isDown( Phaser.KeyCode.RIGHT ) ) {
            
            // Target position 100px to right

            let newX:number = this.sprite.x + 100;
            this.targetPos = newX;
            this.sprite.x = Phaser.Math.linearInterpolation( [ this.sprite.x, newX ], this.paddleMoveSpeed * this.level.game.time.physicsElapsed );
            return;
        }

        // If the mouse has moved adjust its last known position and time

        if( this.level.game.input.x != this.lastKnownMousePos ){
            this.lastKnownMousePos = this.level.game.input.x;
            this.lastMoveTimeofMouse = this.level.game.time.now;
        }
        
        // If the mouse is on the screen and the mouse has moved in the last 100ms switch to mouse control

        if( this.level.game.input.x > 0 && this.level.game.input.x < this.level.game.width 
            && this.level.game.input.y > 0 && this.level.game.input.y < this.level.game.height
                && this.level.game.time.now - this.lastMoveTimeofMouse < 100 ){
            
            // Target position is set to the mouses position
            
            let newX:number = this.sprite.x;
            newX = this.level.game.input.x;

            // The Target position must be within the bounds of the screen
            
            if( this.sprite.x < this.sprite.offsetX ){
                newX = this.sprite.offsetX;
            } else if( this.sprite.x > this.level.game.width - this.sprite.offsetX){
                newX = this.level.game.width - this.sprite.offsetX;
            }

            this.targetPos = newX;
        }

        // If target pos is close enough, set it to current pos to stop jitter from lerp

        if( Math.abs( this.sprite.x - this.targetPos ) < this.level.game.width/800 ){
            this.targetPos = this.sprite.x;
        }

        // If the mouse is clicked release the ball

        if( this.level.input.activePointer.isDown ) {
            this.ball.releaseImmediate();
        }
    }

    /** Add a ball object to this paddle */
    public addBall( ball:Ball ){
        this.ball = ball;
        this.level.game.input.keyboard.onDownCallback = this.ball.release;
        this.level.game.input.keyboard.callbackContext = this.ball;
    }

    /** Return the sprite associated with this object */
    public getSprite(){
        return this.sprite;
    }
}