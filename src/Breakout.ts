import {Preload} from './states/Preload';
import {Menu} from './states/Menu';
import {HighScores} from './states/HighScores';
import {Level} from './states/Level';
import {Editor} from './states/Editor';


export class Breakout extends Phaser.Game {

    public levelCount:number = 3;

    constructor() {
        super( window.innerWidth, window.innerHeight, Phaser.AUTO );

        // full canvas no margin
        
        document.getElementsByTagName( "body" )[0].style.margin = "0";

        this.state.add( 'Preload', Preload, false );
        this.state.add( 'MainMenu', Menu, false );
        this.state.add( 'HighScores', HighScores, false );
        this.state.add( 'Level', Level, false );
        this.state.add( 'Editor', Editor, false );

        this.state.start( 'Preload' );
    }

    
}