/**
 * State for displaying the top 5 highscores on the current machine
 */
export class HighScores extends Phaser.State {
    
    create(){
        // Back button to return to main menu 
        let button = this.game.add.button( this.game.width * 0.05, this.game.height * 0.05, 'brick', this.quit, this);
        let text = this.game.add.text( button.x, button.y, "Back", {fill:"white"} );
        text.anchor.set( 0.5 );
        button.anchor.set( 0.5 );
        button.width = text.width;
        button.height = text.height;
        button.alpha = 0.2;
        
        // If there is no high scores saved on this machine, display "No scores yet" on screen
        if( localStorage.getItem( "highscores" ) === null || JSON.parse( localStorage.getItem( "highscores" ) ).scores.length === 0 ){
            let text = this.game.add.text( this.game.width/2, this.game.height/2, "No scores yet!", {fill:"white"} );
            text.anchor.set( 0.5 );

        } else {

            // Parse scores from local storage and sort in descending order

            let scores = JSON.parse( localStorage.getItem( "highscores" ) ).scores;
            scores.sort( this.compare );

            // Manage position score should be displayed on screen

            let entryHeight = this.game.height * 0.1;
            let startHeight = this.game.height * 0.2;

            // Amount of scores to display
            let limit = scores.length < 5 ? scores.length : 5;

            // Title text and scores
            let text = this.game.add.text( this.game.width/2, startHeight , "Highscores", {fill:"white"} );
            text.anchor.set( 0.5 );

            for ( let i = 0; i < limit; i++) {
                let score = scores[i];
                text = this.game.add.text( this.game.width/2, startHeight + entryHeight*(i+1), score.toString(), {fill:"white"} );
                text.anchor.set( 0.5 );

            }
            
        }
    }
    
    update(){

        // Quit to menu

        if( this.game.input.keyboard.isDown( Phaser.KeyCode.ESC ) ){
            this.quit();
        }
    }

    /** Changes state to the menu state */
    quit(){
        this.game.state.start( "MainMenu" );
    }

    /** Comparator for descending numbers, used to calculate top scores */
    compare( a, b ) {
        if ( a > b) {
            return -1;
        }
        if ( a < b) {
            return 1;
        }
        // a must be equal to b
        return 0;
    }
}