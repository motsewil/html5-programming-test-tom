# README #

### Breakout ###
A simple breakout game with a minimalistic level creation kit.
Goal : Move the paddle to break the all the breakable bricks on the screen, try and gain the most points!

Guide :

 - Bricks can take up to 4 hits from the ball to break and are colored green, yellow, orange and red depending on their strength.
 - Your score will improve if you can consecutively hit bricks without the ball hitting the paddle.
 - You will lose a life if the ball drops off the bottom of the screen, you only have 3 so be careful

### Controls ###

## Keyboard ##

 - Navigate the menus with the arrow keys and enter to confirm
 - Holding down esc will quit from the game to the menu.

 - Arrow keys will move the paddle from left to right on the screen
 - Enter/Space will release the ball from the paddle when it is attached

 ## Mouse/Mobile ##

 - Click menu items to navigate through the menu

 - Click the mouse/ tap the screen to release the ball from the paddle
 - Move the mouse or drag the screen to move the paddle from left to right