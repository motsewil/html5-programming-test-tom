import {Level} from '../states/Level'
import {Paddle} from '../level/Paddle'

/** Class to manage the ball object for the game */
export class Ball {

    /** Size of the ball */
    private scale:number;

    /** Whether the ball has been released from the paddle or not */
    private onPaddle:boolean = true;

    // Scores that the ball has accumulated
    private prevHeldScore = 0;
    private heldScore = 0;

    constructor( public level:Level, private sprite:Phaser.Sprite, private paddle:Paddle ){

        // Size of ball

        this.scale = this.level.game.height * 0.02;
        this.sprite.width = this.scale;
        this.sprite.height = this.scale;
        this.sprite.anchor = new Phaser.Point( 0.5, 0.5 );
        this.sprite.x = this.paddle.getSprite().x;
        this.sprite.y = this.paddle.getSprite().y - this.paddle.getSprite().height;

        // Initialize physics

        this.level.physics.enable( this.sprite, Phaser.Physics.ARCADE );
        this.sprite.checkWorldBounds = true;
        this.sprite.body.collideWorldBounds = true;
        this.sprite.body.bounce.set( 1 );
        this.sprite.events.onOutOfBounds.add( this.level.ballLost , this.level);

        this.paddle.addBall( this );
    }

    /** If the ball should be on the paddle, set its X position to the same as the paddles */
    public update(){
        if( this.onPaddle ) {
            this.sprite.x = this.paddle.getSprite().x;
        }
    }

    /**
     * Attempts to release the ball from the paddle
     * @param e keyboard event that was pressed down
     */
    public release( e ){

        // If the ball is not on the paddle or the level is new do not release

        if( !this.onPaddle || this.level.time.now - this.level.createTime < 100) {
            return;

        }

        // If the keycode was appropriate, release the ball and set onPaddle to false

        if( e.keyCode === Phaser.KeyCode.SPACEBAR || e.keyCode === Phaser.KeyCode.ENTER ){
            this.onPaddle = false;
            this.pushBall();

        }
    }

    /** Releases the ball from the paddle immediately */
    public releaseImmediate() {
        if( this.onPaddle ){
            this.onPaddle = false;
            this.pushBall();
        }
    }

    /** Accumulates score from hitting the brick.
     *  Combos are accumulated by hitting consecutive bricks with no paddleHit breaks
     *  Combos follow the fibonacci sequence
     */
    public hitBrick():number{

        // If this is the first brick hit since the paddle was hit, gain 10 points

        if( this.heldScore === 0 ){
            this.heldScore = 10;
        }
        let oldScore = this.heldScore;
        this.heldScore = this.prevHeldScore + this.heldScore;
        this.prevHeldScore = oldScore;
        return this.heldScore
    }

    /** Returns the score held within the ball and resets the balls current score */
    public retrieveScore():number{
        let finalScore = this.heldScore;
        this.heldScore = 0;
        this.prevHeldScore = 0;
        return finalScore;
    }

    /** Reset the balls to the paddle and clear any velocity and score from the ball */
    public reset(){
        this.onPaddle = true;
        this.heldScore = 0;
        this.prevHeldScore = 0;
        this.sprite.body.velocity.x = 0;
        this.sprite.body.velocity.y = 0;
        this.sprite.x = this.paddle.getSprite().x;
        this.sprite.y = this.paddle.getSprite().y - this.paddle.getSprite().height;
    }

    /** Push the ball away from the paddle at a partially random angle */
    private pushBall(){
        this.sprite.body.velocity.y = -300;
        this.sprite.body.velocity.x = Math.random() * 75;
        this.sprite.body.velocity.x = Math.random() < 0.5 ? -this.sprite.body.velocity.x : this.sprite.body.velocity.x;
    }

    /** Return the sprite associated with the ball */
    public getSprite(){
        return this.sprite;
    }
}