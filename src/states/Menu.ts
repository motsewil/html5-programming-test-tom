export class Menu extends Phaser.State {

    titleStyle:Object = {
        fontSize: 50,
        fill: 'white'
    }
    buttonStyle:Object = {
        fill: 'white'
    }

    titleText:Phaser.Text;
    /** rotation limit for the title text */
    titleTextRotLimit:number = Math.PI/18;
    /** Direction for the rotation of the title text */
    titleTextRotDir:number = 1;
    /** Speed for the rotation of the title text */
    titleTextRotSpeed:number = 0.3;

    playButton:Phaser.Button;
    playButtonText:Phaser.Text;

    scoresButton:Phaser.Button;
    scoresButtonText:Phaser.Text;
    
    resetButton:Phaser.Button;
    resetButtonText:Phaser.Text;

    activeButtonText:Phaser.Text;
    activeButton:Phaser.Button;
    activeFontSize:number = 40;
    inactiveFontSize:number = 15;

    buttons:Phaser.Button[];
    buttonTexts:Phaser.Text[];
    
    

    create() {
        console.log( "state started : " + this.game.state.getCurrentState().key );

        this.game.stage.backgroundColor = '#341d3f';

        // Title Text Setup
        this.titleText = this.game.add.text( this.game.width/2, this.game.height/3.3, "BREAKOUT", this.titleStyle );
        this.titleText.anchor = new Phaser.Point( 0.5, 0.5 );


        // Buttons Setup

        // Play Button Setup

        this.playButton = this.game.add.button( this.game.width*0.5, this.game.height*0.8, "", this.playButtonPressed );
        this.playButton.anchor = new Phaser.Point( 0.5, 1 );

        if( localStorage.getItem( "has-played" ) != null ){
            this.playButtonText = this.game.add.text( this.playButton.x, this.playButton.y, "Continue", this.buttonStyle );
        } else {
            this.playButtonText = this.game.add.text( this.playButton.x, this.playButton.y, "Play", this.buttonStyle );
        }

        this.playButtonText.anchor = new Phaser.Point( 0.5, 1 );
        this.playButtonText.fontSize = this.activeFontSize;

        // High Scores Button Setup

        this.scoresButton = this.game.add.button( this.playButton.x + this.game.width/3.3, this.playButton.y * 0.88, "", this.scoresButtonPressed );
        this.scoresButton.anchor = new Phaser.Point( 0.5, 1 );
        this.scoresButtonText = this.game.add.text( this.scoresButton.x, this.scoresButton.y, "High Scores", this.buttonStyle );
        this.scoresButtonText.anchor = new Phaser.Point( 0.5, 1 );
        this.scoresButtonText.fontSize = this.activeFontSize;

        // Reset Button Setup

        this.resetButton = this.game.add.button( this.playButton.x - this.game.width/3.3, this.playButton.y * 0.88, "", this.resetButtonPressed );
        this.resetButton.anchor = new Phaser.Point( 0.5, 1 );
        this.resetButtonText = this.game.add.text( this.resetButton.x, this.resetButton.y, "Reset Game", this.buttonStyle );
        this.resetButtonText.anchor = new Phaser.Point( 0.5, 1 );
        this.resetButtonText.fontSize = this.activeFontSize;

        // Button Activation
        this.buttonTexts = [ this.resetButtonText, this.playButtonText, this.scoresButtonText ];
        this.buttons = [ this.resetButton, this.playButton, this.scoresButton ];
        this.activeButtonText = this.playButtonText;
        this.activeButton = this.playButton;

        // Input listeners
        this.game.input.keyboard.onDownCallback = this.navigationDown;
        this.game.input.keyboard.callbackContext = this;
        this.game.input.keyboard.addKeyCapture( [ Phaser.Keyboard.LEFT, Phaser.Keyboard.RIGHT, Phaser.Keyboard.ENTER ] );

        
        
    }

    update(){
        this.updateTitleText();
        this.updateButtonPositions();
    }

    /**
     * Is called when the specified keyboard listeners are pressed.
     * Handles Left and Right navigation of the menu and confirmation of selected button
     * @param k keyboard event
     */
    navigationDown( k ){

        if( k.keyCode === Phaser.KeyCode.LEFT ){
            this.setActiveButton( this.buttons[0] );
        }
        if( k.keyCode === Phaser.KeyCode.RIGHT ){
            this.setActiveButton( this.buttons[2] );
        }
        if( k.keyCode === Phaser.KeyCode.ENTER ){
            this.activeButtonPressed();
        }
    }

    /**
     * Handles what button should be 'pressed' when the confirmation key (ENTER) is pressed
     */
    activeButtonPressed(){
        let buttonText = this.activeButtonText.text;

        if( buttonText === "Play" || buttonText === "Continue" ) {
            this.playButtonPressed( this.activeButton );
        } else if ( buttonText === "Reset Game" || buttonText === "Are you sure?"){
            this.resetButtonPressed( this.activeButton );
        } else if ( buttonText === "High Scores" ){
            this.scoresButtonPressed( this.activeButton );
        }
    }

    /**
     * Is called when the button is clicked or tapped via the mouse and handles the function of the button passed via _button
     * @param _button The Phaser.Button that was pressed to call this function
     */
    playButtonPressed( _button ){
        let menu:Menu = this.game.state.getCurrentState() as Menu;

        // if this is the active menu item run its function, otherwise make it the active button
        if( _button === menu.activeButton ){
            menu.game.state.start( "Level" );
        } else {
            menu.setActiveButton( _button );
        }
    }

    /**
     * Is called when the button is clicked or tapped via the mouse and handles the function of the button passed via _button
     * @param _button The Phaser.Button that was pressed to call this function
     */
    scoresButtonPressed( _button ){
        let menu:Menu = this.game.state.getCurrentState() as Menu;

        // if this is the active menu item run its function, otherwise make it the active button
        if( _button === menu.activeButton ){
            menu.game.state.start( "HighScores" );
        } else {
            menu.setActiveButton( _button );
        }
    }

    /**
     * Is called when the button is clicked or tapped via the mouse and handles the function of the button passed via _button
     * @param _button The Phaser.Button that was pressed to call this function
     */
    resetButtonPressed( _button ){
        let menu:Menu = this.game.state.getCurrentState() as Menu;
        
        // if this is the active menu item run its function, otherwise make it the active button
        if( _button === menu.activeButton ){
            if( menu.activeButtonText.text === "Reset Game" ){
                menu.activeButtonText.text = "Are you sure?"
            } else {
                let customLevelCount = localStorage.getItem( "custom-level-count" )
                localStorage.removeItem( "custom-level-count" )
                if( customLevelCount && parseInt( customLevelCount ) > 0  ){
                    for( let i = 0; i < parseInt( customLevelCount ); i++ ) {
                        localStorage.removeItem( "custom-level-" + i+1 );
                    }
                }
                localStorage.removeItem( "custom-first");
                menu.game.state.start( "Preload" );
            }
        } else {
            menu.setActiveButton( _button );
        }
    }

    /**
     * Rearranges both Menu.buttons and Menu.buttonTexts to be in the correct order to respond to their button presses appropriately.
     * Downside of this system is that it only works for 3 button menus, this should be improved to manage expandable menus.
     * @param button the Phaser.Button to make active
     */
    setActiveButton( button ){
        let index = this.buttons.indexOf( button );
        if( index === 1 ){
            return;
        }
        if( this.activeButtonText.text === "Are you sure?"){
            this.activeButtonText.text = "Reset Game";
        } 
    
        // save the current active button in a temporary variable
        let activeIndex = this.buttons.indexOf( this.activeButton );
        let oldActiveButton = this.buttons[ activeIndex ];
        let oldActiveText = this.buttonTexts[ activeIndex ];

        // make the centre of the arrays equal to the new active button
        this.buttons[ activeIndex ] = button;
        this.buttonTexts[ activeIndex ] = this.buttonTexts[ index ];
        
        // make the old button have its place taken by the other inactive button
        this.buttons[ index ] = this.buttons[ Math.abs(index-2) ];
        this.buttonTexts[ index ] = this.buttonTexts[ Math.abs(index-2) ];

        // make the other inactive button take the old active button
        this.buttons[ Math.abs(index-2) ] = oldActiveButton;
        this.buttonTexts[ Math.abs(index-2) ] = oldActiveText;

        // make the centre button and text of the arrays the new active buttons
        this.activeButton = this.buttons[ activeIndex ];
        this.activeButtonText = this.buttonTexts[ activeIndex ];
    }

    /**
     * Called each frame to make sure that the positions and dimensions of each of the buttons are appropriate.
     */
    updateButtonPositions() {
        this.buttonTexts.forEach( buttonText => {
            
            let index:number = this.buttonTexts.indexOf( buttonText );

            // if this button is the active one, lerp towards the active button values. 
            // Otherwise lerp towards inactive fontSize and move to either left or right of the centre depending on this buttons
            // position in the array
            if( this.activeButtonText === buttonText ) {
                buttonText.fontSize = Phaser.Math.linearInterpolation( [ buttonText.fontSize as number , this.activeFontSize ], 0.2 );
                buttonText.x = Phaser.Math.linearInterpolation( [ buttonText.x, this.game.width/2 ], 0.2 );
            } else {
                buttonText.fontSize = Phaser.Math.linearInterpolation( [ buttonText.fontSize as number , this.inactiveFontSize ], 0.2 );
                
                let activeButtonIndex = this.buttonTexts.indexOf( this.activeButtonText );
                let diff = this.game.width/3.3;
                diff = index < activeButtonIndex ? -diff : diff;
                buttonText.x = Phaser.Math.linearInterpolation( [ buttonText.x, this.game.width/2 + diff ], 0.2 );
            }
            // align the buttons to their text
            buttonText.y = this.activeButtonText.y
            this.buttons[ index ].x = buttonText.x;
            this.buttons[ index ].y = buttonText.y;

            // ensure the button is as wide and tall as the text so that it can be pressed whenever the mouse is over the text
            this.buttons[ index ].width = buttonText.width;
            this.buttons[ index ].height = buttonText.height;
        });
    }

    /**
     * Rotate and fluctuate the size of the title text to add some flavor to the menu
     */
    updateTitleText(){
        this.titleText.rotation += this.titleTextRotDir * this.titleTextRotSpeed * this.game.time.physicsElapsed;
        if( Math.abs( this.titleText.rotation ) > this.titleTextRotLimit ) {
            this.titleTextRotDir = this.titleTextRotDir * -1;
        }
        let targetRotation:number = 30 + Math.abs( this.titleText.rotation ) * ( 180/Math.PI );
        // Creates a smooth lerp between the current font size and the desired one
        this.titleText.fontSize = Phaser.Math.linearInterpolation( [ this.titleText.fontSize as number, targetRotation+ 50 ], 20 * this.game.time.physicsElapsed ) ;
    }
}