import {Brick} from '../level/Brick'
import {LevelFormat} from './Level'

/** State for the games level editor */
export class Editor extends Phaser.State {
    
    /** Button to add a new row of bricks */
    private newLineButton:EditorButton;
    
    /** Button for the alignment of the bricks */
    private alignmentButton:EditorButton;
    
    /** Button to save and play the built level */
    private saveAndPlayButton:EditorButton;

    /** 2D Array for rows of + and - buttons for brick creation */
    private rowButtons:EditorButton[][];

    /** 2D Array of brick buttons */
    private bricks:BrickButton[][];

    /** The format of this level */
    private format:LevelFormat = LevelFormat.CENTRE;

    create() {
        console.log( "state started : " + this.game.state.getCurrentState().key );

        // Create the buttons that exist at start of state

        this.newLineButton = new EditorButton( this, this.game.width/2, this.game.height*0.15, 'brick', 'Add new row', this.newRow, this );
        this.alignmentButton = new EditorButton( this, this.game.width/2, ( ( this.game.width * 0.8 )/8 )/3, 'brick', "Centred", this.toggleAlignment, this)
        this.saveAndPlayButton = new EditorButton( this, this.game.width/2, this.game.height * 0.8, "brick", "Save + Play", this.save, this)
        this.rowButtons = [];
        this.bricks = [];
    }


    update(){

        // Move each button to their target position
        this.newLineButton.update();

        let rows = this.bricks;

        for( let row = 0; row < this.bricks.length; row++ ){
            for( let col = 0; col < this.bricks[ row ].length; col++ ){

                let brick = this.bricks[ row ][ col ].brick;
                let sprite = brick.getSprite();
                sprite.anchor = new Phaser.Point( 0.5, 0.5 );

                // Calculate position

                let offsetX = 0;
                let initPointX = 0;
                let amountInRow = rows[ row ].length;
                let bricksFromCentre = col/2;

                // Check whether the level is centre or left aligned

                if( this.format === 0 ){
                    initPointX = this.game.width/2;

                    // If their are an even amount of bricks in this row shift the offset by half a brick width
                    
                    if( amountInRow % 2 === 0 ){
                        offsetX = ( brick.width + brick.buffer ) * Math.floor( bricksFromCentre );
                        offsetX += brick.width/2 + brick.buffer/2;
                    } else {
                        offsetX = ( brick.width + brick.buffer ) * Math.round( bricksFromCentre );
                    }

                    // If the brick should be on the left side of the level, reverse its offset

                    if( col % 2 === 0 ){
                        offsetX *= -1;
                    }
                } else if( this.format === LevelFormat.LEFT ){

                    // Offset is set to brick+buffer width times the column of the brick

                    initPointX = this.game.width * 0.1 - brick.width;
                    offsetX = brick.width + brick.buffer;
                    offsetX *= col+1;
                }
                brick.targetPos = new Phaser.Point( initPointX + offsetX, brick.height*2 + ( brick.height + brick.buffer ) * ( row+1 ) );
                this.bricks[ row ][ col ].brick = brick;
                this.bricks[ row ][ col ].update();
            }
        }
        
    }

    /** Called when the this.alignmentButton is pressed.
     *  Toggles this levels alignment
     */
    public toggleAlignment( ){

        if( this.format === LevelFormat.CENTRE ){

            this.format = LevelFormat.LEFT;
            this.alignmentButton.text.text = "LEFT";
        } else {
            this.format = LevelFormat.CENTRE;
            this.alignmentButton.text.text = "CENTRE";

        }
    }

    /** Adds a new row of +/- buttons for brick creation */
    public newRow(){
        
        // Maximum 8 rows of bricks per level

        if( this.rowButtons.length >= 8 ){
            this.newLineButton.kill();
            return;
        }

        let buttonHeight = ( ( this.game.width * 0.8 )/8 )/3
        let row = this.rowButtons.length+1;
        let y = buttonHeight*2 + (buttonHeight*1.3)*row;

        this.rowButtons[ this.rowButtons.length ] = 
                [ new EditorButton( this, this.game.width * 0.025, y, 'brick', " + ", this.addBrick, this),
                 new EditorButton( this, this.game.width * 0.975, y, 'brick', " - ", this.removeBrick, this) ];
        this.bricks[ this.rowButtons.length-1 ] = [];
        this.newLineButton.targetPosition.y = y + buttonHeight*1.3;
    }

    /**
     * Adds a brick to the row corresponding to the button pressed
     * @param button '+' Button pressed to add a brick
     */
    public addBrick( button ){
        let selectedRow = 0;

        for ( selectedRow = 0; selectedRow < this.rowButtons.length; selectedRow++ ) {
            let addButton = this.rowButtons[ selectedRow ][ 0 ];
            if( addButton.button === button ){

                // Max 8 bricks per row 

                if( this.bricks[ selectedRow ].length >= 8 ){
                    return;
                }

                // Add a brick button to the selected row at default strength

                let brickButton:BrickButton = this.bricks[ selectedRow ][ this.bricks[ selectedRow ].length ] 
                    = new BrickButton(this, new Brick( this, this.add.sprite( button.x, button.y, 'brick' ) ) );
                brickButton.brick.setStrength( 1 );
                break;
            }
        }
    }
    /**
     * Removes a brick to the row corresponding to the button pressed
     * @param button '-' Button pressed to remove a brick
     */
    public removeBrick( button ){
        let selectedRow = 0;
        for ( selectedRow = 0; selectedRow < this.rowButtons.length; selectedRow++ ) {
            let removeButton = this.rowButtons[ selectedRow ][ 1 ];
            if( removeButton.button === button ){
                if( this.bricks[ selectedRow ].length <= 0 ){
                    return;
                }
                this.bricks[ selectedRow ][ this.bricks[ selectedRow ].length-1 ].brick.getSprite().destroy();
                this.bricks[ selectedRow ][ this.bricks[ selectedRow ].length-1 ].button.destroy();
                this.bricks[ selectedRow ][ this.bricks[ selectedRow ].length-1 ].text.destroy();
                this.bricks[ selectedRow ].splice( this.bricks[ selectedRow ].length-1 );
                break;
            }
        }
    }

    /** Save this level to local storage */
    public save(){
        let jsonObject = {
            format: this.format,
            bricks: []
        };
        
        // Create the JSON Object for this level

        for ( let index = 0; index < this.bricks.length; index++ ) {
            jsonObject.bricks[ index ] = [];
            let row = this.bricks[index];
            for ( let i = 0; i < row.length; i++ ) {
                let brickButton = row[i];
                jsonObject.bricks[ index ][ i ] = brickButton.brick.getStrength();
            }

        }

        // Find how many custom levels already exist
        let levelCount:number|string = localStorage.getItem( "custom-level-count" );
        if( !levelCount ) {
            levelCount = 1;
        } else {
            levelCount = parseInt( levelCount ) + 1;
        }

        // Save to local storage

        localStorage.setItem( "custom-level-count", levelCount.toString() );
        localStorage.setItem( "custom-level-" + levelCount.toString(), JSON.stringify( jsonObject ) );
        localStorage.setItem( "custom-first", "1" );
        this.game.state.start( "Level" );
    }
}

/** Brick button for the editor so attributes of a brick can be saved */
export class BrickButton {

    /** Button to increment the bricks strength */
    public button:Phaser.Button;
    /** Text to display the bricks strength */
    public text:Phaser.Text;

    /** Strength of the brick associated with this button */
    private strength:number = 1;
    /** Texts to display for the strength of the brick */
    private strengthTexts:string[] = [ "0", "1", "2", "3", "4", "Infinite" ];

    constructor( public state:Phaser.State, public brick:Brick ) {

        // Create brick and display text

        this.button = state.add.button( brick.getSprite().x , brick.getSprite().y, 'brick', this.click, this );
        this.button.alpha = 0;
        this.button.anchor = new Phaser.Point( 0.5, 0.5 );
        this.text = state.add.text( brick.getSprite().x, brick.getSprite().y, '1', {} );
        this.text.anchor = new Phaser.Point( 0.5, 0.5 );
    }

    /** Update the position of the brick and the button/text attached */
    update(){
        this.brick.update();
        this.button.width = this.brick.width;
        this.button.height = this.brick.height;
        this.button.position = this.brick.getSprite().position;
        this.text.position = this.brick.getSprite().position;
    }
    
    /** Increment the strength of this brick and display its new text */
    public click(){
        this.strength++;
        if( this.strength >= this.strengthTexts.length ){
            this.strength = 0;
        }
        this.text.text = this.strengthTexts[ this.strength ];
        this.brick.setStrength( this.strength );
    }
}

/** Convenience class for simple buttons with text inside,
 */
export class EditorButton {

    /** Phaser to button to be handled when clicked */
    public button:Phaser.Button;
    /** Text to display on top of the button */
    public text:Phaser.Text;
    

    public targetPosition:Phaser.Point;

    private defaultStyle = {
        fill: 'black'
    }

    /**
     * Creates Phaser.Text and Phaser.Button and adds them to the scene, button scale is set to 1.1 times the size of the text dimensions
     * @param state this state for the button
     * @param x horizontal coordinate for the button
     * @param y vertical coordinate for the button
     * @param key image key for the button background
     * @param textContent string for the button
     * @param callback callback for button click
     * @param callbackContext context for callback
     */    
    constructor( public state:Phaser.State, public x:number, public y:number, public key:string, public textContent:string, public callback:Function,callbackContext){
        this.button = state.add.button( x, y, key, callback, callbackContext );
        this.text = state.add.text( x,y,textContent, this.defaultStyle );
        this.button.width = this.text.width * 1.1;
        this.button.height = this.text.height * 1.1;
        this.button.anchor = new Phaser.Point( 0.5, 0.5 );
        this.button.alpha = 0.2;
        this.text.anchor = this.button.anchor;
        this.targetPosition = new Phaser.Point( this.button.x, this.button.y );
    
    }

    /** Update the buttons position towards their target position */
    public update(){
        this.button.x = Phaser.Math.linearInterpolation( [this.button.x, this.targetPosition.x], 0.2);
        this.button.y = Phaser.Math.linearInterpolation( [this.button.y, this.targetPosition.y], 0.2);
        this.text.position = this.button.position;

        if( this.button.y > this.state.game.height * 0.7 ) {
            this.kill();
        }

        this.x = this.button.x;
        this.y = this.button.y;
    }
    
    /** kill both the button and text associated with this class */
    public kill(){
        this.button.kill();
        this.text.kill();
    }
}